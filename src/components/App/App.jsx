// Dependencies
import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import './App.css';
// Components
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
// Pages
import Home from '../../pages/Home';
import Project from '../../pages/Project';



/**
 * The root component. Handles the template and navigation.
 */
const App = () =>
    <div>
        <BrowserRouter>
            <Route path='/' component={Header} />
            <Route exact path='/' component={Home} />
            <Route exact path='/project/:slug' component={Project} />
            <Route path='/' component={Footer} />
        </BrowserRouter>
    </div>



export default App;