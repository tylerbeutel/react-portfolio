import React from 'react';
import './Button.css';



/**
 * A button with basic configuration.
 * 
 * @param {string} label - The button label.
 * @param {string} link - The button link.
 * @param {boolean} newTab - The button link.
 */
const Button = ({ label, link, newTab }) => {
    const target = newTab ? '_blank' : '_self';

    return (
        <a href={link} className='btn' target={target} >
            {label}
        </a>
    );
}



export default Button;