import React from 'react';
import { Link } from 'react-router-dom'
import './Footer.css';



/**
 * The footer component.
 */
const Footer = () => 
    <footer>
        Built by Tyler Beutel  |  &copy; <Link to="/">tylerbeutel.com</Link>
    </footer>



export default Footer;