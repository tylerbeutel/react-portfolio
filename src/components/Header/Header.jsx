import React from 'react';
import './Header.css';
import { Link } from 'react-router-dom';



/**
 * The header component.
 */
const Navigation = () =>
    <header>
        <Link className='logo' to='/' >Tyler Beutel.</Link>
    </header>



export default Navigation;