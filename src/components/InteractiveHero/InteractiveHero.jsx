import React, { Component } from 'react';
import './InteractiveHero.css';
import './Stars.css';
import Button from '../Button/Button';
import randomNo from '../../helpers/randomNumber';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner'   ;



/**
 * A component built as a hero section.
 */
class InteractiveHero extends Component {
    state = {
        starmap: [],
        shootingstars: [
            //left, top, delay
            [-5,  20, 0],
            [-5, -10, 2],
            [-5,  55, 6]
        ],
        mouseXY: [0, 0]
    }


    /**
     * Lifecycle method. 
     * Runs once after the component mounts.
     */
    componentDidMount = () => {
        // Map the stars and add to state
        const starmap = [];
        for (let i=0; i<70; i++) {
            //               x-postion       y-position       intensity      distance     animate-delay
            starmap[i] = [randomNo(100,0), randomNo(100,0), randomNo(6,2), randomNo(5,2), randomNo(6,0)]
        };
        this.setState({ starmap });
    }


    /**
     * Handles the rendering of stars and passes each star their properties.
     */
    renderStars = () => {
        if (this.state.starmap.length > 0) {
            return this.state.starmap.map(star => {

                // divide the movement by twice the intensity
                let diffX = -( this.state.mouseXY[0] / (star[3]*2) );
                let diffY = -( this.state.mouseXY[1] / (star[3]*2) );

                return (
                    <div className='star' style={{ 
                        height: star[2],
                        width: star[2],
                        left: `${star[0] + diffX}%`,
                        top: `${star[1] + diffY}%`,
                        animationDelay: `${star[4]}s`
                    }} />
                );

            });
        }
    }


    /**
     * Find position of mouse as a percentage relative to the window.
     */
    distanceFromCenter = (x, y) => [
        Math.floor( (x/window.innerWidth) *100 ), 
        Math.floor( (y/window.innerHeight) *100 )
    ];



    /**
     * Configures and renders the shooting stars.
     */
    renderShootingStars = () => {
        return this.state.shootingstars.map(star => {
            return (
                <div className="shootingstar" style={{ 
                    top: `${star[0]}%`,
                    left: `${star[1]}%`,
                    animationDelay: `${star[2]}s`
                }}  />
            );
        })
    }


    /**
     * 
     */
    renderCenterCircles = () => {
        let diffY = -( this.state.mouseXY[1] / 40 );
        let diffX = -( this.state.mouseXY[0] / 40 );

        return (
            <div className='circles' >
                <div className='circle-border' style={{ 
                    transition: '0s',
                    transform: `translate(${diffX*1.5}%, ${diffY*1.5}%)`
                }} />
                <div className='circle-filled' style={{ 
                    transition: '0s',
                    transform: `translate(${diffX}%, ${diffY}%)`
                }} />
            </div>
        )
    }


    /**
     * The default render method.
     */
    render() {
        return(
            <div className='interactivehero'>
                <div className='darkbg' onMouseMove={({ clientX: x, clientY: y }) => this.setState({ mouseXY: this.distanceFromCenter(x, y) })}>
                    {this.renderStars()}
                    {this.renderShootingStars()}
                    {this.renderCenterCircles()}
                    <div className='content-box'>
                        <h1>welcome to my <span>portfolio</span></h1>
                        <Button label='Hand Coded' link='#handcoded-portfolio' />
                        <Button label='WordPress' link='#wordpress-portfolio' />
                    </div>
                </div>
            </div>
        );
    }
}



export default InteractiveHero;