import React from 'react';
import './LoadingSpinner.css';


const LoadingSpinner = () => {
    return (
        <div className="loader" >
            <div className="center2" />
            <div className="center1" /> 
            <div className="orbit orbit1" /> 
            <div className="orbit orbit2" /> 
            <div className="orbit orbit3" />
        </div>
    );
}


export default LoadingSpinner;