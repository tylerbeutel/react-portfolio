import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './ProjectCard.css';



/**
 * A component for displaying portfolio projects as cards.
 * 
 * @param {Object} project - Custom WordPress Rest API post-type response. 
 *      WP post-type must contain a slug, title and large featured image.
 */
class ProjectCard extends Component {
    state = { cardLink: '' }


    /**
     * Lifecycle method. 
     * Runs once after the component mounts.
     */
    componentDidMount = () => {
        return this.setState({ cardLink: `/project/${ this.props.project.slug }` });
    }


    /**
     * Handles the rendering of the card header.
     */
    renderCardHeader = () => 
        <div className="projectcard-header" >
            <Link to={this.state.cardLink}>
                <h3>{ this.props.project.title.rendered }</h3>
            </Link>
            <Link to={this.state.cardLink} className="projectcard-header-arrow" >
                &#8250;
            </Link>
        </div>


    /**
     * Handles the rendering of the card image.
     */
    renderCardBody = () =>
        <Link to={this.state.cardLink}>
            <img 
                src={this.props.project._embedded['wp:featuredmedia']['0'].media_details.sizes.medium_large.source_url}
                className="projectcard-img"
            />
        </Link>


    /**
     * Default render method.
     */
    render = () =>
        <div className="projectcard" key={ this.props.project.id } >
            {this.renderCardHeader()}
            {this.renderCardBody()}
        </div>

}



export default ProjectCard;