/* A random number from 0 -> X and offset by Y */
const randomNo = (x, y) => {
    return Math.floor( (Math.random() * x ) + y);
}


export default randomNo;