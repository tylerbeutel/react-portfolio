import React, { Component, Fragment } from 'react'
import axios from 'axios'
import keys from '../config/keys';
import InteractiveHero from '../components/InteractiveHero/InteractiveHero';
import ProjectCard from '../components/ProjectCard/ProjectCard';
import LoadingSpinner from '../components/LoadingSpinner/LoadingSpinner';



/**
 * The home page.
 */
class Home extends Component {
    state = {
        wordpressProjects: [],
        handcodedProjects: []
    }


    /**
     * Lifecycle method. 
     * Runs once after the component mounts.
     */
    componentDidMount = async () => {
        // Make request for posts.
        const response = await axios.get( `${keys.BACKEND_API_URL}/wp-json/wp/v2/portfolio?_embed`);

        // Seperate 'WordPress' pojects
        const wordpressProjects = response.data.filter(project => 
            project._embedded['wp:term']['0']['0'].name === "WordPress"
        );

        // Seperate 'Hand Coded' pojects
        const handcodedProjects = response.data.filter(project => 
            project._embedded['wp:term']['0']['0'].name === "Hand Coded"
        );

        // Add sorted projects to state.
        this.setState({ wordpressProjects, handcodedProjects })
    }

  
    /**
     * The default render method.
     */
    render() {
        const { handcodedProjects, wordpressProjects } = this.state;

        return (
            <Fragment>
                <InteractiveHero />

                <div className="container">
                    <h2 id="handcoded-portfolio">Hand-Coded Projects</h2>
                    { handcodedProjects.length > 0 
                        ? <div className="card-coloumns" >
                            { handcodedProjects.map( project => <ProjectCard project={project} /> )}
                          </div> 
                        : <LoadingSpinner /> 
                    }
                </div>

                <div className="container" >
                    <h2 id="wordpress-portfolio">WordPress sites for clients</h2>
                    { wordpressProjects.length > 0
                        ? <div className="card-coloumns" >
                            { wordpressProjects.map( project => <ProjectCard project={project} /> )}
                          </div>
                        : <LoadingSpinner />
                    }
                </div>
                
            </Fragment>
        )
    }

}



export default Home;