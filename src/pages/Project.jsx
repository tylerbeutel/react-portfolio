import React, { Component } from 'react';
import axios from 'axios';
import keys from '../config/keys';
import Button from '../components/Button/Button'
import LoadingSpinner from '../components/LoadingSpinner/LoadingSpinner';



/**
 * The single project page.
 */
class Project extends Component {
    state = { 
        project: {},
        tech: {}
    }

    /**
     * Lifecycle method. 
     * Runs once after the component mounts.
     */
    componentDidMount = async () => {
        // Make request for posts.
        const slug = this.props.match.params.slug;
        const api = keys.BACKEND_API_URL;
        const response = await axios.get( `${api}/wp-json/wp/v2/portfolio?slug=${slug}&_embed` );

        // Pass response to the state
        this.setState({ project: response.data[0] });
    }

    
    /**
     * The default render method.
     */
    render() {
        // If request data has populated the state
        if ( Object.keys(this.state.project).length > 0 ) {

            // Set variables for readability
            const projectTitle = this.state.project.title.rendered;
            const projectContent = this.state.project.content.rendered;
            const demoLink = this.state.project.acf.demo_link;
            const demoCodeLink = this.state.project.acf.demo_code_link;

            return (
                <div className="white-background" >  
                    <div className="container project-flex" >

                        <div className="project-flexitem">
                            <img 
                                src={this.state.project._embedded['wp:featuredmedia']['0'].media_details.sizes.full.source_url}
                                className="projectcard-img"
                            />
                        </div>

                        <div className="project-flexitem">
                            <h1>{projectTitle}</h1>
                            
                            <p
                                style={{ marginTop: 10, marginBottom: 10 }}
                                dangerouslySetInnerHTML={{
                                    __html: projectContent
                                }} 
                            />
                            
                            {   // Show demo button if it exists
                                this.state.project.acf.demo_link &&
                                <Button label="Demo" link={demoLink} newTab />
                            }
                            
                            {   // Show demo code button if it exists
                                this.state.project.acf.demo_code_link &&
                                <Button label="Demo Code" link={demoCodeLink} newTab />
                            }
                        </div>
                        
                    </div>
                </div>
            );
        } else {
            return <LoadingSpinner />;
        }
    }
}



export default Project;